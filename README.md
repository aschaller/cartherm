Description
===========
The cartherm project is a simple car thermometer that will periodically measure
ambient temperature external to your vehicle. It includes a quadalphanumeric
LED display with brightness control, several I/O buttons, and other
miscillaneous components.


Features
--------
This embedded projects supports the following features:    

 *  Switch between degrees fahrenheit and degrees Celsius    
 *  Vary the brightness of the quadalphanumeric LED display    
 *  Briefly display the average temperature for the week    


Contact
-------
It is highly encouraged if you have ideas, comments, or questions regarding
this project that you contact me - I would be glad to hear from you.
